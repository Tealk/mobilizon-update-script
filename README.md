# shell-administration-scripts
This script executes the update commands as described on https://docs.joinmobilizon.org/administration/upgrading/release/.

## Usage
1) Upload the "config" and the "update" File to your server.
2) Give the "update" File execute rights one so you can run it.
3) Run "./update" and choose from the menu

### Screens
Menü
![Menü](https://framagit.org/Tealk/mobilizon-update-script/-/raw/b665948c48bb6fe2ca8f99d0a513b62acb0b68f7/screen/menu.jpg)

Successful run
![Progress](https://framagit.org/Tealk/mobilizon-update-script/-/raw/b665948c48bb6fe2ca8f99d0a513b62acb0b68f7/screen/progress.jpg)

## Extensions/improvements
Create a report [here](https://framagit.org/Tealk/mobilizon-update-script/-/issues) and surprise me with your ideas.

## Bug Report
Feel free to create a report [here](https://framagit.org/Tealk/mobilizon-update-script/-/issues)
